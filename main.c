// Implémentation de la fonction main.

#include "include/main.h"

#ifdef TEST
#include "include/tests.h"
#else
#include "include/app.h"
#endif


//Point d'entrée de l'application.
int main()
{
    #ifdef TEST
        test_all();
    #else
        init();
    #endif
    return 0;
}




