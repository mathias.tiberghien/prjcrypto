// Implémentation des fonctios d'encryption et décryption.


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "include/crypto.h"
#include "include/main.h"
#include "include/fileio.h"
#include "include/helpers.h"

// Fonction d'encryption. Le fichier source est lu, encrypté à l'aide du perroquet et l'encryption est enregistré dans un fichier.
int encrypt(const char* path, const char* phrase)
{
    fflush(stdin);
    int count = 0;
    int len = strlen(phrase);
    // Ouverture des fichiers sources et destinations.
    FILE* f_source = open_file(path, "r");
    FILE* f_dest = open_file(DEST_FILE, "wb");
    // Si les fichiers ont été ouverts correctement.
    if(f_source != NULL && f_dest != NULL)
    {
        char c;
        fread(&c, sizeof(char), 1, f_source);
        // Si le fichier source n'est pas vide.
        if(feof(f_source))
        {
            #ifndef TEST
            printf(CRYPTO_FILE_EMPTY);
            printf(APP_OP_CANCELED);
            #endif
            return count;
        }
        #ifndef TEST
        printf(CRYPTO_ENCRYPTING);
        #endif
        int i_perroq = 0;
        char enc_c;
        // Boucle d'encodage basé sur le module du nombres de caracteres lu par la longueur de la phrase.
        while(!feof(f_source))
        {
            enc_c = c-phrase[i_perroq];
            fread(&c, sizeof(char), 1, f_source);
            fwrite(&enc_c, sizeof(char), 1, f_dest);
            count++;
            i_perroq = count%len;
        }
        #ifndef TEST
        printf(CRYPTO_NB_ENCRYPTED, count);
        #endif
        close_file(f_source);
        // Le fichier source est supprimé seulement si le fichier encrypté a été sauvegardé correctement.
        if(close_file(f_dest) == 0)
        {
            #ifndef TEST
            printf(CRYPTO_FILE_SAVED, DEST_FILE);
            #endif
            delete_file(path, 0);
        }
    }
    else
    {
        #ifndef TEST
        printf(APP_OP_CANCELED);
        #endif
        if(f_source!=NULL)
        {
            close_file(f_source);
        }
        if(f_dest!=NULL)
        {
            close_file(f_dest);
        }
    }
    return count;
}

// Opération de décryption.
int decrypt(const char* path, const char* phrase)
{
    int count = 0;
    #ifndef TEST
    printf(CRYPTO_DECRYPTING, DEST_FILE);
    #endif
    int len = strlen(phrase);
    FILE* f_crypt = open_file(DEST_FILE, "rb");
    FILE* f_source = open_file(path, "w");
    if(f_source != NULL && f_crypt !=NULL)
    {
        char c;
        fread(&c, sizeof(char), 1, f_crypt);
        if(feof(f_crypt))
        {
            #ifndef TEST
            printf(CRYPTO_FILE_EMPTY);
            printf(APP_OP_CANCELED);
            #endif
            return count;
        }
        int i_perroq = 0;
        char dec_c;
        // La boucle de décryption est similaire à l'encryption sauf le perroquet est ajouté au caractère encrypté.
        while(!feof(f_crypt))
        {
            dec_c = c+phrase[i_perroq];
            fwrite(&dec_c, sizeof(char), 1, f_source);
            fread(&c, sizeof(char), 1, f_crypt);
            if(c == 127)
            {
                c=26;
            }
            count++;
            i_perroq = count%len;
        }
        #ifndef TEST
        printf(CRYPTO_NB_DECRYPTED, count);
        #endif
        close_file(f_crypt);
        // Le fichier encrypté est supprimé seulement si le fichier décrypté est bien enregistré.
        if(close_file(f_source) == 0)
        {
            #ifndef TEST
            printf(CRYPTO_DECRYPT_SUCCESS, DEST_FILE, path);
            #endif
            delete_file(DEST_FILE, 0);
        }
    }
    else
    {
        #ifndef TEST
        printf(APP_OP_CANCELED);
        #endif
        if(f_source!=NULL)
        {
            close_file(f_source);
        }
        if(f_crypt!=NULL)
        {
            close_file(f_crypt);
        }
    }
    return count;
}

// Opération de lecture du perroquet dans le fichier dédié.
char* read_perroq()
{
    char* phrase = read_all_file(PERROQ_FILE);
    if(phrase != NULL)
    {
        return phrase;
    }
    else
    {
        #ifndef TEST
        printf(CRYPTO_PARROT_NOT_FOUND);
        #endif
        return NULL;
    }

}

// LRécupération de la saisie du perroquet et écriture dans le fichier dédié.
short write_perroq()
{
    char phrase[LPHRASE+1];
    printf(APP_INVITE_PARROT);
    get_phrase(phrase, LPHRASE, HIDDEN);
    if(strlen(phrase)==1 && (phrase[0]==CANCEL || phrase[0]==CANCEL_CAPS))
    {
        printf(APP_OP_CANCELED);
        return 0;
    }
    save_perroq(phrase);
    return 0;
}

// Ecriture du fichier perroq dans le fichier dédié. Renvoit 1 en cas de succès.
short save_perroq(char* phrase)
{
    FILE* f = open_file(PERROQ_FILE, "w");
    if(f!=NULL)
    {
        fwrite(phrase, sizeof(char), strlen(phrase), f);
        close_file(f);
        return 1;
    }
    return 0;
}

// Vérifie que le perroquet du fichier est le même que celui fourni par l'utilisateur. Renvoit 1 si le test est satisfaisan.
short check_perroq(const char* true_phrase, const char* usr_phrase)
{
    short res = strcmp(true_phrase, usr_phrase)==0;
    if(!res)
    {
        #ifndef TEST
        printf(CRYPTO_PARROT_NO_MATCH);
        #endif
    }
    return res;
}
