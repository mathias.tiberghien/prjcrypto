// Implémentation de fonctions utilitaires.
#include <stdio.h>
#include "include/main.h"

#ifdef _WIN32
#include <conio.h>
#else
#include "include/conio.h"
#endif

// Récupération d'une phrase dans une chaîne fournie en paramètre, dont la taille maximale est fournie en paramètre.
// Le paramètre hide permet de cacher la saisie à l'utilisateur (1) ou non (0).
void get_phrase(char* phrase, int max_size, short hide)
{
    fflush(stdin);
    char c;
    int i=0;
    do
    {
        c = getch();
        // On ignore les flèches et caractères d'échappement.
        if(c==0 || c==-32 || c=='\033')
        {
            getch();
            if (c=='\033')
            {
                getch();
            }
            continue;
        }
        // Prise en charge de la touche delete.
        if(c==8 || c==127)
        {
            if(i>0)
            {
                printf("\b \b");
                i--;
            }
            continue;
        }
        // Arrêt de saisie sur retour chariot.
        if(c!='\r' && c!='\n')
        {
            if(i<max_size)
            {
                phrase[i++]=c;
                // Le caractère est éventuellement masqué par le symbole '*'.
                printf("%c", (hide == HIDDEN ? '*': c) );
            }
            else
            {
                continue;
            }
        }
        else
        {
            phrase[i]='\0';
            printf("\n");
        }

    } while (c!='\r' && c!='\n');
    fflush(stdin);
}
