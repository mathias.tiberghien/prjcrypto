## Tâches réalisées

- Création d'une application permettant à l'exécution des commandes suivantes:
    - e pour encrypter un fichier. S'il n'existe pas de perroquer l'utilisateur est invité à en créé un
    - d pour décrypter un fichier. Possible uniquement s'il existe un fichier encrypté
    - c permet de configurer le perroquet. Possible uniquement s'il n'existe pas de fichier encrypté
    - q permet de quitter l'application
    - ? permet d'afficher l'aide

**Remarques**: 
- Il n'est possible d'encrypter qu'un seul fichier à la fois
- On peut choisir le chemin du fichier à encrypter et le chemin du fichier décrypté
- Si on essaye d'encrypter un fichier alors qu'il existe déjà un fichier encrypté on peut choisir de le décrypter
- Lorsque q'un fichier est encrypté correctement la source est détruite
- Lorsque q'un fichier est décrypté correctement le fichier encrypté est détruit
- On ne peut pas modifier le perroquet tant qu'il existe un fichier encrypté
- Chaque fois qu'on tente de décrypter un fichier le perroquet est demandé à l'utilisateur
- Si le fichier de décryption existe l'utilisateur peut choisir de l'écraser ou non
- Il est possible d'utiliser les fichiers source.txt et test.txt siutés à la racine du projé pour tester l'encryption

Le code et chaque méthode est commenté et séparé par fonctionnalités.

### Structure du projet
Le projet est décomposé en plusieurs modules:
- main : l'application principale
- app : les fonctions liées à l'interaction utilisateur
- fileio : les opérations sur les fichiers
- crypto : les opérations d'encryption
- helpers : les opérations utilitaires

Les headers sont dans le dossier /include situé à la racine du projet.
Les fichiers de ressources langue sont dans le dossier /ressources situé à la racine du projet.

### Tests unitaires
Il est possible de réaliser un jeu de 10 tests unitaires, testant les fonctions d'opérations sur les fichiers, et les opérations d'encryption/décryption
Pour réaliser les tests unitaires il suffit de décommenter la ligne de code suivante dans le fichier `main.h`

`
#define TEST
`
Des opérations conditionnelles de préprocesseur permettent d'exécuter les tests quand ce symbole est défini. En commentant la ligne l'application est exécutée normalement.

L'implémentation des tests est effectuée dans des fichier ayant le pattern {nom_de_module}.tests.c
Le fichier tests.c implémente l'exécution des tests unitaires.

### Langues
Il est possible de configurer la langue de l'application en définissant le symbole `LANGUAGE` dans le fichier `main.h`

`
#define LANGUAGE FR
`
ou
`
#define LANGUAGE EN
`

Les tests et l'application s'exécuteront dans la langue choisie.

## Description du cahier des charges

On souhaite à partir d’un fichier source, faire en sorte de le crypter en utilisant l’algorithme du
« perroquet ».
Cet algorithme repose sur un mot (le perroquet) qui permet de crypter en calculant la différence
ASCII caractère par caractère.
Illustration du principe du perroquet :
Mot source à crypter Olivier
Perroquet abcd
Résultat en calcul ascii (‘O’-‘a’)(‘l’-‘b’)(‘i’-‘c’)(‘v’-‘d’)(‘i’-‘a’)(‘e’-‘b’)(‘r’-‘c’)
Résultat Concaténation des codes ASCII
L’utilisateur devra lui-même définir son propre perroquet.
Celui qui crypte et qui décrypte doit donc connaître le mot (ou la phrase) du « perroquet ». On
prévoira donc un fichier (« peroq.def ») contenant la chaine de caractères du perroquet.
On disposera de 2 fichiers.
Source (« source.txt ») : contenant le texte à crypter
Résultat (« dest.crt ») : contenant le texte crypté
De plus, on prévoira de supprimer la source après cryptage de telle manière à ne conserver que
le fichier crypté.
Afin de faciliter l’utilisation de votre programme, vous proposerez à l’utilisateur un menu
simple et convivial implémentant l’ensemble des fonctionnalités.
A l’issue de la durée du projet, vous déposerez dans la section "Travaux", le projet dans sa
globalité. Vous y mettrez le fichier zippé de votre projet ainsi que tous les fichiers nécessaires
à son exécution. Vous mettrez également un fichier texte contenant le lien vers votre dépôt
distant.
CONSEIL : En vue de faciliter la réutilisation des éléments, créer des fonctions indépendantes
en leur définissant pour chacune un rôle bien précis.

----

Vous utiliserez un dépôt distant afin de sauvegarder l'ensemble de vos fichiers et d'assurer le
suivi. Les fichiers suivis seront uniquement les .c et .h.

