// Implémentation des opération d'écriture, lecture et suppression de fichier.

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "include/main.h"

// Ouverture d'un fichier dont le chemin et le mode d'accès sont passés en paramètre.
FILE* open_file(const char* path, const char* acces_mode )
{
    FILE* f = fopen(path, acces_mode);
    if(f==NULL)
    {
        printf(FIO_READ_ERROR, path);
        return NULL;
    }
    return f;
}

// Fermeture du fichier. Renvoit 1 en cas d'erreur.
short close_file(FILE* f)
{
    if (fclose(f)!=0)
    {
        #ifndef TEST
        printf(FIO_WRITE_ERROR);
        #endif
        return 1;
    }
    return 0;
}

// Suppression d'un fichier don le chemin est passé en paramètre. Le paramètre silent permet d'afficher un message de confirmation (0) ou non (1).
// Renvoit 1 en cas de succes
short delete_file(const char* path, short silent)
{
    if(remove(path)==0)
    {
        if(silent != SILENT)
        {
            #ifndef TEST
            printf(FIO_FILE_DELETED, path);
            #endif
        }
        return 1;
    }
    else
    {
        #ifndef TEST
        printf(FIO_ERROR_DELETE, path);
        #endif
    }
    return 0;
}

// Vérification de l'existence du fichier dont le chemin est passé en paramètre. Le paramètre silent permet d'afficher un message de confirmation (0) ou non (1).
// Renvoit 1 si le fichier existe.
short check_file_exist(const char* path, short silent)
{
    short result = 0;
    if (access(path, F_OK) == 0)
    {
      result = 1;
    }
    if(silent != SILENT)
    {
        printf(FIO_FILE_BE_NO_BE, path, result == 0 ? FIO_NO_EXSIT:FIO_EXIST);
    }
    return result;
}

// Lecture de l'entierté d'un fichier texte. la chaîne est allouée dynamiquement. Renvoit NULL en cas d'echec.
char* read_all_file(const char* path)
{
    FILE* f = open_file(path, "r");
    if(f!=NULL)
    {
        // On mesure la taille de la phrase a récupérer avant d'affecter la taille de la chaîne de retour.
        fseek(f, 0, SEEK_END);
        long fsize = ftell(f);
        fseek(f, 0, SEEK_SET);
        char* text = malloc(fsize + 1);
        fread(text, fsize, 1, f);
        text[fsize]='\0';
        close_file(f);
        return text;
    }
    return NULL;
}
