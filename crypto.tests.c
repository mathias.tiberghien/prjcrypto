// Implémentation des tests des fonctions de crypto.c

#include "include/main.h"

#ifdef TEST

#include "include/crypto.h"
#include "include/fileio.h"
#include <stdlib.h>
#include <string.h>

// Teste si la validation du perroquet saisi par l'utilisateur fonctionne.
short test_check_perroq()
{
   char p1[] = "Bonjour";
   char p2[] = "bonjour";
   char p3[] = "Bonjour";

   short res = check_perroq(p1, p2); 
   if(res!=0)
   {
      return EXIT_FAILURE;
   }

   res = check_perroq(p1, p3);
   if(res!=1)
   {
     return EXIT_FAILURE;
   }

   return EXIT_SUCCESS;
}

// Teste la sauvegarde d'un perroquet.
short test_save_perroq()
{
    char p1[] = "DevIsFun";
    return save_perroq(p1) == 1 ? EXIT_SUCCESS : EXIT_FAILURE;
}

// Teste la lecture du perroquet.
short test_read_perroq()
{
   char* phrase = read_perroq();
   short res = EXIT_FAILURE;
   if(phrase != NULL)
   {
      res = strcmp("DevIsFun", phrase) == 0 ? EXIT_SUCCESS : EXIT_FAILURE;
      free(phrase);
      phrase = NULL;
   }
   return res;
}

// Teste l'encryption d'un fichier.
short test_encrypt()
{
   short res = EXIT_FAILURE;
   char* text = F_TEST_TEXT;
   char* phrase = read_perroq();
   if(phrase != NULL)
   {
      int text_size = strlen(text);
      int nb_encrypted = encrypt(F_TEST, phrase);
      res = nb_encrypted == text_size ? EXIT_SUCCESS : EXIT_FAILURE;
      free(phrase);
      phrase = NULL;
   }

   return res;
}

// Test la decryption d'un fichier.
short test_decrypt()
{
   short res = EXIT_FAILURE;
   char* text = F_TEST_TEXT;
   char* phrase = read_perroq();
   if(phrase != NULL)
   {
      int text_size = strlen(text);
      int nb_decrypted = decrypt(F_TEST, phrase);
      res = nb_decrypted == text_size ? EXIT_SUCCESS : EXIT_FAILURE;
      free(phrase);
      phrase = NULL;
      if(res == EXIT_SUCCESS)
      {
         char* text_dec = read_all_file(F_TEST);
         if(text_dec != NULL)
         {
            res = strcmp(text, text_dec) == 0 ? EXIT_SUCCESS : EXIT_FAILURE;
            free(text_dec);
            text_dec = NULL;
         }
         else
         {
            res = EXIT_FAILURE;
         }
      }
   }

   return res;
}

#endif