// Implémentation des tests des fonctions de fileio.c

#include "include/main.h"

#ifdef TEST
#include "include/fileio.tests.h"
#include "include/fileio.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// Teste l'ouverture d'un fichier en écriture et en lecture.
short test_open_file()
{
    FILE* f = open_file(F_TEST, "w");
    short res = f == NULL ? EXIT_FAILURE : EXIT_SUCCESS;
    if(res == EXIT_SUCCESS)
    {
        fclose(f);
        f = open_file(F_TEST, "r");
        res = f == NULL ? EXIT_FAILURE : EXIT_SUCCESS;
        if (res == EXIT_SUCCESS)
        {
            fclose(f);
        }
    }
    return res;
}

// Teste la fermeture d'un fichier.
short test_close_file()
{
    FILE* f = fopen(F_TEST, "r");
    short res = close_file(f) == 0 ? EXIT_SUCCESS : EXIT_FAILURE;
    return res;
}

// Teste la vérification de l'existence d'un fichier.
short test_check_file_exist()
{
    short res = check_file_exist(F_TEST, SILENT) == 1 ? EXIT_SUCCESS : EXIT_FAILURE;
    if(res == EXIT_SUCCESS)
    {
        res = check_file_exist("dummy_file_should_not_exist.test", SILENT) == 0 ? EXIT_SUCCESS : EXIT_FAILURE;
    }
    return res;
}

// Teste la suppression d'un fichier.
short test_delete_file()
{
    short res = delete_file(F_TEST, SILENT) == 1 ? EXIT_SUCCESS : EXIT_FAILURE;
    return res;
}

// Teste la récupération de l'entierté du contenu d'un fichier dans une chaîne de caractères.
short test_read_all_file()
{
    short res = EXIT_FAILURE;
    char text[] = F_TEST_TEXT;
    FILE* f = fopen(F_TEST, "w");
    if (f != NULL)
    {
        fwrite(text, sizeof(char), strlen(text), f);
        fclose(f);
    }
    char* text2 = read_all_file(F_TEST);
    if(text2 != NULL)
    {
        res = strcmp(text, text2) == 0 ? EXIT_SUCCESS: EXIT_FAILURE;
        free(text2);
        text2 = NULL;
    }
    return res;
}

#endif

