#ifndef RESSOURCES_FR_H_INCLUDED
#define RESSOURCES_FR_H_INCLUDED

#define APP_WELCOME "Bienvenue dans le programme d'encryption perroquet.\n"
#define APP_MENU_TITLE "Menu des actions:\n"
#define APP_MENU_E "La touche 'e' permet d'encrypter un fichier.\n"
#define APP_MENU_D "La touche 'd' permet de decrypter un fichier encrypte.\n"
#define APP_MENU_C "La touche 'c' permet de configurer le perroquet.\n"
#define APP_MENU_Q "La touche 'q' permet de quitter l'application.\n"
#define APP_MENU_H "La touche '?' permet d'afficher ce menu.\n"
#define APP_ASK_ACTION "\nQue voulez-vous faire ('?' pour voir l'aide):"
#define APP_BYE "\nMerci d'avoir utilise l'application. A bientot!\n"
#define APP_WRONG_CHOICE "Ce choix n'est pas permis, veuiller taper '?' pour plus d'information\n"
#define APP_ENCRYPT_TITLE "Encryption d'un fichier.\n"
#define APP_ENCRYPT_WARNING "Attention, le fichier source sera supprime apres encryption.\n"
#define APP_ENCRYPT_EXISTS "Presence d'un fichier encrypte. Voulez-vous le decrypter (o/n)?"
#define APP_ENCRYPT_FILE "Veuillez renseignez le fichier a encrypter ('a' pour annuler): "
#define APP_OP_CANCELED "Operation annulee.\n"
#define APP_CONFIG_TITLE "Configuration du perroquet.\n"
#define APP_CONFIG_ENCRYPTED "Un fichier est actuellement encrypte avec une autre clef. Operation impossible.\n"
#define APP_DECRYPT_TITLE "Decryption d'un fichier.\n"
#define APP_DECRYPT_NO_ENCRYPTED "Aucun fichier encrypte. "
#define APP_INVITE_PARROT "Veuillez renseigner la phrase du perroquet ('a' pour annuler): "
#define APP_INVITE_DECRYPT "Veuillez renseignez le fichier de decryptage ('a' pour annuler): "
#define APP_DECRYPT_EXISTS "Le fichier '%s' existe deja. L'ecraser (o/n)?"
#define APP_PARROT_NO_EXISTS "Le perroquet n'existe plus pour ce fichier. Operation impossible.\n"
#define CRYPTO_FILE_EMPTY "Le fichier '%s', est vide. "
#define CRYPTO_ENCRYPTING "Encryption du fichier...\n"
#define CRYPTO_NB_ENCRYPTED "%d caracteres ont ete encryptes.\n"
#define CRYPTO_FILE_SAVED "Le fichier '%s' a ete enregistre avec succes.\n"
#define CRYPTO_DECRYPTING "Decryption du fichier '%s'.\n"
#define CRYPTO_NB_DECRYPTED "%d caracteres ont ete decryptes.\n"
#define CRYPTO_DECRYPT_SUCCESS "Le fichier '%s' a ete decrypte avec succes et enregistre dans le fichier '%s'.\n"
#define CRYPTO_PARROT_NOT_FOUND "Le perroquet n'a pas pu etre retrouve.\n"
#define CRYPTO_PARROT_NO_MATCH "La phrase est incorrecte, veuilez recommencer.\n"
#define FIO_READ_ERROR "Erreur lecture fichier '%s'!\n"
#define FIO_WRITE_ERROR "Erreur fermeture fichier '%s'!\n"
#define FIO_FILE_DELETED "Le fichier '%s' a ete supprime pour plus de securite.\n"
#define FIO_ERROR_DELETE "Une erreur est survenue lors de la suppression du fichier '%s'. Veuillez le supprimer manuellement.\n"
#define FIO_FILE_BE_NO_BE "Le fichier '%s' %s.\n"
#define FIO_EXIST "existe"
#define FIO_NO_EXSIT "n'existe pas"
#define TESTS_TITLE "Routine d'execution de tests de l'application d'encryption perroquet.\n"
#define TESTS_PASS "succes"
#define TESTS_FAIL "echec"
#define TESTS_EXECUTING "Execution test '%s': "
#define TESTS_REVIEW "%d tests realises. %d tests echoues.\n"
#define YES 'o'
#define NO 'n'
#define CANCEL 'a'
#define YES_CAPS 'O'
#define NO_CAPS 'N'
#define CANCEL_CAPS 'A'
#endif // RESSOURCES_FR_H_INCLUDED
