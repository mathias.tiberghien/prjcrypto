#ifndef RESSOURCES_EN_H_INCLUDED
#define RESSOURCES_EN_H_INCLUDED

#define APP_WELCOME "Welcome to parrot encryption program.\n"
#define APP_MENU_TITLE "Menu:\n"
#define APP_MENU_E "'e' to encrypt a file.\n"
#define APP_MENU_D "'d' to decrypt an encrypted file.\n"
#define APP_MENU_C "'c' to configure the parrot.\n"
#define APP_MENU_Q "'q' to quit the program.\n"
#define APP_MENU_H "'?' displays the current menu.\n"
#define APP_ASK_ACTION "\nWhat do you whant to do ('?' displays help):"
#define APP_BYE "\nThanks for using the application. See you soon!\n"
#define APP_WRONG_CHOICE "Wrong choice, use '?' for more information\n"
#define APP_ENCRYPT_TITLE "Encrypting a file.\n"
#define APP_ENCRYPT_WARNING "Be aware that the source file will be deleted after encryption.\n"
#define APP_ENCRYPT_EXISTS "There is already an encrypted file. Decrypt (y/n)?"
#define APP_ENCRYPT_FILE "Please set the path of the file to be encrypted ('c' to cancel): "
#define APP_OP_CANCELED "Operation aborted.\n"
#define APP_CONFIG_TITLE "Parrot Settings.\n"
#define APP_CONFIG_ENCRYPTED "A file is currently encrypted with another key. Operation denied.\n"
#define APP_DECRYPT_TITLE "Decrypting a file.\n"
#define APP_DECRYPT_NO_ENCRYPTED "No encrypted file. "
#define APP_INVITE_PARROT "Please set parrot's phrase ('c' to cancel): "
#define APP_INVITE_DECRYPT "Please set the path for the decrypted file ('c' to cancel): "
#define APP_DECRYPT_EXISTS "File '%s' already exists. Overwrite (y/n)?"
#define APP_PARROT_NO_EXISTS "The parrot doesn't exists anymore. Operation denied.\n"
#define CRYPTO_FILE_EMPTY "File '%s', is empty. "
#define CRYPTO_ENCRYPTING "Encrypting file...\n"
#define CRYPTO_NB_ENCRYPTED "%d chars have been encrypted.\n"
#define CRYPTO_FILE_SAVED "File '%s' was successfully saved.\n"
#define CRYPTO_DECRYPTING "Decrypting file '%s'.\n"
#define CRYPTO_NB_DECRYPTED "%d chars have been decrypted.\n"
#define CRYPTO_DECRYPT_SUCCESS "File '%s' has been decrypted successfully and save in file '%s'.\n"
#define CRYPTO_PARROT_NOT_FOUND "The parrot wasn't found.\n"
#define CRYPTO_PARROT_NO_MATCH "Phrase is incorrect. Please, try again.\n"
#define FIO_READ_ERROR "Error while reading file '%s'!\n"
#define FIO_WRITE_ERROR "Error while closing file '%s'!\n"
#define FIO_FILE_DELETED "File '%s' was deleted for safety reasons.\n"
#define FIO_ERROR_DELETE "An error occured while deleting file '%s'. Please try to delete it manually.\n"
#define FIO_FILE_BE_NO_BE "File '%s' %s.\n"
#define FIO_EXIST "exists"
#define FIO_NO_EXSIT "does not exist"
#define TESTS_PASS "pass"
#define TESTS_FAIL "failed"
#define TESTS_EXECUTING "Executing test '%s': "
#define TESTS_REVIEW "%d executed tests. %d failed tests.\n"
#define TESTS_TITLE "Tests execution routine for parrot encryption application.\n"
#define YES 'y'
#define NO 'n'
#define CANCEL 'c'
#define YES_CAPS 'Y'
#define NO_CAPS 'N'
#define CANCEL_CAPS 'C'

#endif // RESSOURCES_EN_H_INCLUDED
