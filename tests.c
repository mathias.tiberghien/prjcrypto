// Implémentation de l'exécution des tests unitaires.

#include "include/main.h"

#ifdef TEST

#include <stdio.h>
#include <stdlib.h>
#include "include/tests.h"
#include "include/crypto.tests.h"
#include "include/fileio.tests.h"

// Définition de la liste des tests à éxécuter
test tests[] = {
    {"test_open_file", test_open_file},
    {"test_close_file", test_close_file},
    {"test_check_file_exist", test_check_file_exist},
    {"test_delete_file", test_delete_file},
    {"test_read_all_file", test_read_all_file},
    {"test_check_perroq", test_check_perroq},
    {"test_save_perroq", test_save_perroq},
    {"test_read_perroq", test_read_perroq},
    {"test_encrypt", test_encrypt},
    {"test_decrypt", test_decrypt}
    };

// Teste tous les tests définis plus haut. Indique pour chaque test, la réussite ou le succes.
void test_all()
{
    printf(TESTS_TITLE);
    int errors = 0;
    int nb_tests = sizeof(tests)/sizeof(test);
    short res;
    for(test* t = tests; t < tests + nb_tests; t++)
    {
        printf(TESTS_EXECUTING, t->name);
        res = t->execute();
        errors+= res;
        printf("%s", res == EXIT_FAILURE ? TESTS_FAIL: TESTS_PASS);
        printf("\n");

    }
    // Affiche le nombre total de tests et réalisés et le nombre d'échecs.
    printf(TESTS_REVIEW, nb_tests, errors);
}

#endif
