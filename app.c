// Implémentation de la logique d'application (menu, aide, choix des actions).

#ifdef _WIN32
#include <conio.h>
#else
#include "include/conio.h"
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "include/fileio.h"
#include "include/main.h"
#include "include/crypto.h"
#include "include/app.h"
#include "include/helpers.h"

//Initialisation de l'application.
void init()
{
    printf(APP_WELCOME);
    display_menu();
    action_selection();
}

//Affichage du menu d'aide.
void display_menu()
{
    printf("\n");
    printf(APP_MENU_TITLE);
    printf(APP_MENU_E);
    printf(APP_MENU_D);
    printf(APP_MENU_C);
    printf(APP_MENU_Q);
    printf(APP_MENU_H);
    printf("\n");
}

//Selection d'une action du menu.
void action_selection()
{
    fflush(stdin);
    printf(APP_ASK_ACTION);
    char action = getch();
    printf("%c\n", action);
    switch(action)
    {
        case 'e':
        case 'E': encrypt_action(); break;
        case 'd':
        case 'D': decrypt_action(); break;
        case 'c':
        case 'C': config_action(); break;
        case 'q':
        case 'Q': printf(APP_BYE); return;
        case '?': display_menu(); break;
        default: printf(APP_WRONG_CHOICE);
    }
    action_selection();
}


// Tâche d'encryption.
void encrypt_action()
{
    printf(APP_ENCRYPT_TITLE);
    printf(APP_ENCRYPT_WARNING);
    fflush(stdin);
    // Si le perroquet a été configuré.
    if(check_file_exist(PERROQ_FILE, SILENT) == 1)
    {
        // S'il existe déjà un fichier encrypté on propose de le décrypter.
        if(check_file_exist(DEST_FILE,SILENT)==1)
        {
            printf(APP_ENCRYPT_EXISTS);
            char rep = getch();
            printf("%c\n", rep);
            fflush(stdin);
            if(rep == YES || rep == YES_CAPS)
            {
                decrypt_action();
            }
        }
        char path[LPATH+1];
        // Récupération d'un fichier source existant.
        do
        {
            printf(APP_ENCRYPT_FILE);
            get_phrase(path, LPATH, 0);
            if(strlen(path) == 1 && (path[0]==CANCEL || path[0]== CANCEL_CAPS))
            {
                return;
            }
        }while(check_file_exist(path, 0)==0);
        //Récupération du perroquet
        char* phrase = read_perroq();
        if(phrase != NULL)
        {
            encrypt(path, phrase);
            free(phrase);
            phrase = NULL;
        }
        else
        {
            printf(APP_OP_CANCELED);
        }
    }
    // Sinon l'utilisateur doit configurer le perroquet.
    else
    {
        if(config_action()==1)
        {
            encrypt_action();
        }
    }
}

// Action de configuration du perroquet.
short config_action()
{
    printf(APP_CONFIG_TITLE);
    // Si un fichier est déjà encrypté, l'opération est impossible.
    if(check_file_exist(DEST_FILE, SILENT)==1)
    {
        printf(APP_CONFIG_ENCRYPTED);
        return 0;
    }
    return write_perroq();
}

// Action de decryption du fichier
void decrypt_action()
{
    printf(APP_DECRYPT_TITLE);
    // L'opération est possible seulement si un fichier encrypté existe.
    if(check_file_exist(DEST_FILE, SILENT) == 0)
    {
        printf(APP_DECRYPT_NO_ENCRYPTED);
        printf(APP_OP_CANCELED);
        return;
    }
    // L'utilisateur doit confirmer le perroquet pour décrypter le fichier.
    if(check_file_exist(PERROQ_FILE, SILENT) == 1)
    {
        char* true_phrase = read_perroq();
        if(true_phrase != NULL)
        {
            char phrase[LPHRASE+1];
            // Tant que l'utilisateur n'a pas renseigné le perroquet ou annuler il doit resaisir le perroquet.
            do
            {
                fflush(stdin);
                printf(APP_INVITE_PARROT);
                get_phrase(phrase, LPHRASE, HIDDEN);
                if(strlen(phrase)==1 && (phrase[0]==CANCEL || phrase[0]== CANCEL_CAPS))
                {
                    printf(APP_OP_CANCELED);
                    free(true_phrase);
                    true_phrase = NULL;
                    return;
                }
            }while(check_perroq(true_phrase, phrase)==0);
            char path[LPATH+1];
            short overwrite;
            short file_exists;
            // L'utilisateur renseigne le fichier de décryption à créer (ou remplacer)
            // tant que le fichier existe, ou que l'utiliseur accepte d'écraser le fichier ou qu'il annule.
            do
            {
                overwrite = 0;
                fflush(stdin);
                printf(APP_INVITE_DECRYPT);
                get_phrase(path, LPATH, 0);
                if(strlen(path) == 1 && (path[0]==CANCEL || path[0]==CANCEL_CAPS))
                {
                    printf(APP_OP_CANCELED);
                    free(true_phrase);
                    true_phrase = NULL;
                    return;
                }
                file_exists = check_file_exist(path, SILENT);
                if(file_exists == 1)
                {
                    printf(APP_DECRYPT_EXISTS, path);
                    char rep = getch();
                    printf("%c\n", rep);
                    fflush(stdin);
                    if(rep == YES || rep == YES_CAPS)
                    {
                        overwrite=1;
                    }
                }
            } while (file_exists && !overwrite);
            
            // Operation de decryptage.
            decrypt(path, true_phrase);
            // Libération de la mémoire.
            free(true_phrase);
            true_phrase = NULL;
        }
        else
        {
            printf(APP_OP_CANCELED);
        }
    }
    else
    {
        printf(APP_PARROT_NO_EXISTS);
    }
}
