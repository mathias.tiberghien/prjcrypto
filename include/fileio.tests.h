// Déclaration des fonctions de tests pour fileio.c

#ifndef FILEIO_TESTS_H_INCLUDED
#define FILEIO_TESTS_H_INCLUDED

short test_open_file();
short test_close_file();
short test_delete_file();
short test_check_file_exist();
short test_read_all_file();

#endif // FILEIO_TESTS_H_INCLUDED