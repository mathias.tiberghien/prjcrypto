// Déclaration des fonctions de crypto.c

#ifndef CRYPTO_H_INCLUDED
#define CRYPTO_H_INCLUDED

#define PERROQ_FILE "perroq.def"
#define SOURCE_FILE "source.txt"
#define DEST_FILE "dest.crt"

int encrypt(const char* source_path, const char* perroquet);
int decrypt(const char* decrypt_path, const char* perroquet);
char* read_perroq();
short write_perroq();
short save_perroq(char* perroquet);
short check_perroq(const char* perroquet_1, const char* perroquet_2);

#endif // CRYPTO_H_INCLUDED
