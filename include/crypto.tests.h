// Déclaration des fonctions de tests.c

#ifndef CRYPTO_TESTS_H_INCLUDED
#define CRYPTO_TESTS_H_INCLUDED

short test_check_perroq();
short test_save_perroq();
short test_read_perroq();
short test_encrypt();
short test_decrypt();
void decrypt(const char* path, const char* phrase);

#endif // CRYPTO_TESTS_H_INCLUDED