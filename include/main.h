// Déclaration des fonctions communes.

#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

#define SILENT 1
#define HIDDEN 1
#define LPATH 200
#define LPHRASE 100

//En décommentant la ligne ci-dessous on peut lancer une batterie de tests unitaires sur les fonctions de crypto.
//#define TEST

#ifdef TEST
#define F_TEST "test_open.txt"
#define F_TEST_TEXT "Bonjour Monde!"
#endif

#define FR 1
#define EN 2
#define CULTURE FR

#endif // MAIN_H_INCLUDED

#if CULTURE == FR
#include "../ressources/ressources.fr.h"
#else
#include "../ressources/ressources.en.h"
#endif
