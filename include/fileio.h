// Déclaration des fonctions de fileio.c

#ifndef FILEIO_H_INCLUDED
#define FILEIO_H_INCLUDED

#include <stdio.h>

FILE* open_file(const char* path , const char* acces_mode);
short close_file(FILE* fp);
short delete_file(const char* path, short silent);
short check_file_exist(const char*, short silent);
char* read_all_file(const char* path);

#endif // FILEIO_H_INCLUDED
