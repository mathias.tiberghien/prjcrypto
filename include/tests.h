// Déclaration des fonctions de test globales

#ifndef TESTS_H_INCLUDED
#define TESTS_H_INCLUDED

// Une structure de test pour permettre l'exécution de tests unitaire (voir fonction tests.c)
typedef struct test
{
    char name[100];
    short (*execute)();
} test;

void test_all();

#endif // CRYPTO_TESTS_H_INCLUDED
