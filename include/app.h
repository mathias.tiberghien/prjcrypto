// Déclaration des fonctions de app.c

#ifndef APP_H_INCLUDED
#define APP_H_INCLUDED

void action_selection();
void display_menu();
void encrypt_action();
short config_action();
void decrypt_action();
void init();

#endif // APP_H_INCLUDED
